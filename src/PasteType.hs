-- This module only exists because of the GHC stage restriction.

{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module PasteType where

import Database.Persist.TH
import GHC.Generics
import Data.Aeson

data PasteType = PlainText deriving (Show, Read, Eq, Generic)

instance FromJSON PasteType where
    parseJSON (String "plain") = return PlainText

instance ToJSON PasteType where
    toJSON PlainText = "plain"

derivePersistField "PasteType"